using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpButtonCode : MonoBehaviour
{
    public Text showText;
    public int codeText;
    public int currentNum = 0;

    // Start is called before the first frame update
    void Start()
    {
        showText.text = "" + currentNum;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddNumber()
    {
        currentNum += 1;
        if (currentNum == 10)
        {
            currentNum = 0;
        }
        showText.text = "" + currentNum;
    }
}
