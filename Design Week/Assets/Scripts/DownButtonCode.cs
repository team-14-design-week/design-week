using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownButtonCode : MonoBehaviour
{
    public Text showText;

    public int currentNum = 0;
    public GameObject upButton;

    // Start is called before the first frame update
    void Start()
    {

        UpButtonCode upButtonCode = upButton.GetComponent<UpButtonCode>();
        showText.text = "" + upButtonCode.currentNum;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SubNumber()
    {

        UpButtonCode upButtonCode = upButton.GetComponent<UpButtonCode>();
        upButtonCode.currentNum -= 1;
        if (upButtonCode.currentNum == -1)
        {
            upButtonCode.currentNum = 9;
        }
        showText.text = "" + upButtonCode.currentNum;
    }
}
