using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectCode : MonoBehaviour
{
    public GameObject code1;
    public GameObject code2;
    public GameObject code3;
    public GameObject code4;

    public bool correctCode = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpButtonCode num1 = code1.GetComponent<UpButtonCode>();
        UpButtonCode num2 = code2.GetComponent<UpButtonCode>();
        UpButtonCode num3 = code3.GetComponent<UpButtonCode>();
        UpButtonCode num4 = code4.GetComponent<UpButtonCode>();
        if (num1.currentNum == num1.codeText)
        {
            if (num2.currentNum == num2.codeText)
            {
                if (num3.currentNum == num3.codeText)
                {
                    if (num4.currentNum == num4.codeText)
                    {
                        correctCode = true;
                    }
                    else
                    {
                        correctCode = false;
                    }
                }
                else
                {
                    correctCode = false;
                }
            }
            else
            {
                correctCode = false;
            }
        }
        else
        {
            correctCode = false;
        }
    }
}
