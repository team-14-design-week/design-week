using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //Declaring Variables and components
    public Rigidbody2D rb;
    private Animator anim;


    public float movementSpeed = 5f;
    
    bool facingRight = true;
    private bool playerMoving = false;
    private Vector2 lastMove;

    Vector2 movement;

    private static bool playerExists;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
 
    }//end of Start

    // Update is called once per frame
    void Update()
    {
        PlayerInput();
    }//end of Update

    private void FixedUpdate()
    {
        //Player Movement
        rb.MovePosition(rb.position + movement * movementSpeed * Time.fixedDeltaTime);

    }//end of FixedUpdate

    // Character Movement
    void PlayerInput() 
    {

        playerMoving = false;

        //Player Movement Input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        //flips player model according to directions
        if (movement.x > 0f && !facingRight)
            Flip();//flip right when looking left
        else if (movement.x < 0f && facingRight)
            Flip();//flip left when looking right

        //checks player movement
        if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
        {
            playerMoving = true;
            lastMove = new Vector2(Input.GetAxisRaw("Horizontal"), 0f);
        }
        if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
        {
            playerMoving = true;
            lastMove = new Vector2(0f, Input.GetAxisRaw("Vertical"));
        }

        //player Animation
        anim.SetFloat("MovementX", Input.GetAxisRaw("Horizontal"));
        anim.SetFloat("MovementY", Input.GetAxisRaw("Vertical"));
        anim.SetFloat("LastMoveX", lastMove.x);
        anim.SetFloat("LastMoveY", lastMove.y);
        anim.SetBool("PlayerMoving", playerMoving);

    }//end of PlayerInput

    //flips player
    void Flip()
    {
        facingRight = !facingRight;

        //turns player model
        Vector3 theScale = transform.localScale;
        theScale.x *= -1f;

        transform.localScale = theScale;
    }//end of Flip
}
