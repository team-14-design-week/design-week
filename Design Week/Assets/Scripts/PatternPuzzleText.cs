using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PatternPuzzleText : MonoBehaviour
{
    public Text letter;
    public bool canWrite = false;
    public bool priorLetter = false;

    public string correctLetter;

    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;

    // Start is called before the first frame update
    void Start()
    {

        letter.text = "";

    }

    // Update is called once per frame
    void Update()
    {
        if (canWrite == true)
        {
            WriteLetter();
        }

    }

    public void ChangeColorGreen()
    {

        GetComponent<Image>().color = Color.green;

        PatternPuzzleText patternCode2 = button2.GetComponent<PatternPuzzleText>();
        PatternPuzzleText patternCode3 = button3.GetComponent<PatternPuzzleText>();
        PatternPuzzleText patternCode4 = button4.GetComponent<PatternPuzzleText>();
        PatternPuzzleText patternCode5 = button5.GetComponent<PatternPuzzleText>();



        patternCode2.GetComponent<Image>().color = Color.gray;
        patternCode3.GetComponent<Image>().color = Color.gray;
        patternCode4.GetComponent<Image>().color = Color.gray;
        patternCode5.GetComponent<Image>().color = Color.gray;

        canWrite = true;

        patternCode2.canWrite = false;
        patternCode3.canWrite = false;
        patternCode4.canWrite = false;
        patternCode5.canWrite = false;
    }

    public void WriteLetter()
    {


        foreach (char c in Input.inputString)
        {
            if (priorLetter == true)
            {
                letter.text = "";
            }
            letter.text += c;
            priorLetter = true;
        }


    }
}
