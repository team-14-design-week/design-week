using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectPerspective : MonoBehaviour
{
    public bool correctPatternCode = false;

    public GameObject button1;
    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PatternPuzzleText patternCode1 = button1.GetComponent<PatternPuzzleText>();
        PatternPuzzleText patternCode2 = button2.GetComponent<PatternPuzzleText>();
        PatternPuzzleText patternCode3 = button3.GetComponent<PatternPuzzleText>();
        PatternPuzzleText patternCode4 = button4.GetComponent<PatternPuzzleText>();
        PatternPuzzleText patternCode5 = button5.GetComponent<PatternPuzzleText>();

        if(patternCode1.letter.text == "h" || patternCode1.letter.text == "H")
        {
            if (patternCode2.letter.text == "e" || patternCode2.letter.text == "E")
            {
                if (patternCode3.letter.text == "l" || patternCode3.letter.text == "L")
                {
                    if (patternCode4.letter.text == "l" || patternCode4.letter.text == "L")
                    {
                        if (patternCode5.letter.text == "o" || patternCode5.letter.text == "O")
                        {
                            correctPatternCode = true;
                        }
                    }
                }
            }
        }
    }
}
