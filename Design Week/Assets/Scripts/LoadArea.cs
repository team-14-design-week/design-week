using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadArea : MonoBehaviour
{

    public string LevelToLoad;

    //to exit puzzles
    public void LoadScene()
    {
        SceneManager.LoadScene(LevelToLoad);
    }

    //for puzzle scenes
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.F))
                SceneManager.LoadScene(LevelToLoad);

            Debug.Log("onArea");
        }
    }//end of OnTriggerEnter2D

}
